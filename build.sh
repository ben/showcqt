#!/bin/bash -e

rm -rf dist
mkdir dist

PATH=/opt/wasi-sdk/bin clang -std=c11 -Ofast -flto -fvisibility=hidden -Wl,--no-entry -Wl,--strip-all -Wl,--export-dynamic -Wl,-allow-undefined-file=showcqt_native/wasm.syms -Wl,-lto-O3 -Wall -Wextra -Werror -Wdouble-promotion -o showcqt.wasm showcqt_native/*.c
chmod -x showcqt.wasm

wasm_hash=$(sha256sum showcqt.wasm | cut -d ' ' -f 1)
mv showcqt.wasm dist/showcqt.$wasm_hash.wasm

sed -e "s#%WASM_HASH%#$wasm_hash#g" showcqt.js > dist/showcqt.js
js_hash=$(sha256sum dist/showcqt.js | cut -d ' ' -f 1)
mv dist/showcqt.js dist/showcqt.$js_hash.js

css_hash=$(sha256sum showcqt.css | cut -d ' ' -f 1)
cp showcqt.css dist/showcqt.$css_hash.css

css_hash_b64=$(echo "$css_hash" | xxd -r -p | base64)
js_hash_b64=$(echo "$js_hash" | xxd -r -p | base64)

sed -e "s#%CSS_HASH%#$css_hash#g" -e "s#%JS_HASH%#$js_hash#g" -e "s#%CSS_HASH_B64%#$css_hash_b64#g" -e "s#%JS_HASH_B64%#$js_hash_b64#g" index.html > dist/index.html

zopfli -v dist/*
