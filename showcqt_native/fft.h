/*

See fft.c for copyright.

*/

typedef float FFTSample;
typedef struct FFTComplex
{
	FFTSample re, im;
} FFTComplex;

void fft_init();
void av_fft_permute(FFTComplex *z);
void av_fft_calc(FFTComplex *z);

#define COSTABLE(size) const FFTSample ff_cos_##size[size/2]
extern COSTABLE(16);
extern COSTABLE(32);
extern COSTABLE(64);
extern COSTABLE(128);
extern COSTABLE(256);
extern COSTABLE(512);
extern COSTABLE(1024);
extern COSTABLE(2048);
extern COSTABLE(4096);
extern COSTABLE(8192);
