"use strict";

(async function() {
	const WIDTH = 1920;
	const HEIGHT = 1080;

	if (!window.AudioContext && window.webkitAudioContext) {
		console.log("using webkitAudioContext instead of AudioContext");
		window.AudioContext = webkitAudioContext;
		for (let key in AudioContext.prototype) {
			if (!key.startsWith("webkit")) {
				continue;
			}

			let nonVendor = key.substr("webkit".length, 1).toLowerCase() + key.substr("webkit".length + 1);
			if (!AudioContext.prototype[nonVendor]) {
				AudioContext.prototype[nonVendor] = AudioContext.prototype[key];
			}
		}
	}
	if (window.File && !File.prototype.arrayBuffer && window.FileReader) {
		console.log("shimming File.arrayBuffer");
		File.prototype.arrayBuffer = function() {
			let f = this;

			return new Promise(function(resolve, reject) {
				let fr = new FileReader();

				fr.onload = function() {
					resolve(fr.result);
				};
				fr.onerror = function() {
					reject(fr.error);
				};

				fr.readAsArrayBuffer(f);
			});
		};
	}

	const wasmPromise = fetch("showcqt.%WASM_HASH%.wasm");

	const canvas = document.createElement("canvas");
	canvas.className = "screen";
	canvas.width = WIDTH;
	canvas.height = HEIGHT;
	document.body.appendChild(canvas);
	const cctx = canvas.getContext("2d", {
		alpha: false,
		desynchronized: true
	});

	const actx = new AudioContext();
	const cqtNode = actx.createScriptProcessor(2048);

	// ensure cqt is constantly updated by continuously sending it nothing
	const silence = actx.createConstantSource ? actx.createConstantSource() : actx.createOscillator();
	const sgain = actx.createGain();
	silence.connect(sgain);
	sgain.gain.setValueAtTime(0, actx.currentTime);
	sgain.connect(cqtNode);
	silence.start();

	// ensure cqt is called by "playing" it at 0% volume
	const gain = actx.createGain();
	gain.gain.setValueAtTime(0, actx.currentTime);
	gain.connect(actx.destination);
	cqtNode.connect(gain);

	let image_data, audio_data, filter_frame;
	const {module, instance} = await WebAssembly.instantiateStreaming(wasmPromise, {
		env: {
			send_frame: () => cctx.putImageData(image_data, 0, 0),
		}
	});
	instance.exports.init_showcqt(actx.sampleRate);
	let img_data_buf = new Uint8ClampedArray(instance.exports.memory.buffer,
		instance.exports.get_image_data_ptr(), WIDTH * HEIGHT * 4);
	image_data = new ImageData(img_data_buf, WIDTH, HEIGHT);
	audio_data = new Float32Array(instance.exports.memory.buffer,
		instance.exports.get_audio_data_ptr(), 4096);
	filter_frame = instance.exports.filter_frame;
	let updateAudioPlaying;
	cqtNode.addEventListener("audioprocess", function(e) {
		let input = e.inputBuffer;
		let channels = e.inputBuffer.numberOfChannels;
		let left = input.getChannelData(0);
		let right = input.getChannelData(channels - 1);
		audio_data.set(left, 0);
		audio_data.set(right, 2048);
		filter_frame(1, left.length);

		updateAudioPlaying(e.playbackTime);
	});

	let nextTime = 0;
	const upload = document.createElement("input");
	upload.className = "upload";
	upload.type = "file";
	upload.accept = "audio/*";
	upload.multiple = true;
	upload.onchange = async function() {
		for (let f of upload.files) {
			let buffer = await f.arrayBuffer();
			let decoded = await actx.decodeAudioData(buffer);
			let node = actx.createBufferSource();
			node.buffer = decoded;
			node.connect(actx.destination);
			node.connect(cqtNode);

			nextTime = Math.max(nextTime, actx.currentTime);
			node.start(nextTime);
			nextTime += decoded.duration;
		}
		upload.value = null;
	};

	const form = document.createElement("form");
	form.className = "form";
	document.body.appendChild(form);

	function addButton(cls, title, icon, click) {
		const btn = document.createElement("button");
		btn.className = cls;
		btn.title = title;
		btn.textContent = icon;
		btn.addEventListener("click", function(e) {
			e.preventDefault();
			click();
		});
		form.appendChild(btn);
	}

	addButton("invert-colors-dark", "Dark Mode", "\u{1F319}\uFE0E", function() {
		document.body.parentNode.className = "dark";
	});
	addButton("invert-colors-light", "Light Mode", "\u{2600}\uFE0E", function() {
		document.body.parentNode.className = "light";
	});
	addButton("upload-files", "Upload Files", "\u{1F4E4}\uFE0E", function() {
		upload.click();
	});
	const mic = {
		device: null,
		node: null,
	};
	addButton("use-microphone", "Use Microphone", "\u{1F3A4}\uFE0E", async function() {
		if (mic.device) {
			mic.node.disconnect(cqtNode);
			mic.device.getTracks().forEach(t => t.stop());
			mic.device = null;
			return;
		}

		mic.device = await navigator.mediaDevices.getUserMedia({
			audio: {
				advanced: [
					{
						channelCount: 2
					}
				]
			}
		});
		mic.node = actx.createMediaStreamSource(mic.device);
		mic.node.connect(cqtNode);
	});
	if (canvas.captureStream && document.pictureInPictureEnabled) {
		const pipVideo = document.createElement("video");
		pipVideo.srcObject = canvas.captureStream();
		addButton("picture-in-picture", "Picture-in-Picture", "\u{1F4FA}\uFE0E", async function() {
			if (document.pictureInPictureElement) {
				await document.exitPictureInPicture();
				return;
			}

			await pipVideo.play();
			await pipVideo.requestPictureInPicture();
		});
	}

	updateAudioPlaying = function(currentTime) {
		form.classList.toggle("playing-audio", mic.device || currentTime < nextTime);
	};

	if (location.hash) {
		let url = decodeURIComponent(location.hash.substr(1));
		if (url.startsWith("https://")) {
			let req = await fetch(url, {
				mode: "cors",
				headers: {
					"Accept": "audio/*"
				}
			});
			let buffer = await req.arrayBuffer();
			let decoded = await actx.decodeAudioData(buffer);
			let node = actx.createBufferSource();
			node.buffer = decoded;
			node.connect(actx.destination);
			node.connect(cqtNode);

			nextTime = Math.max(nextTime, actx.currentTime);
			node.start(nextTime);
			nextTime += decoded.duration;
		}
	}

	document.getElementById("loading").hidden = true;
})();
